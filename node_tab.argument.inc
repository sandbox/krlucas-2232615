<?php

/**
 * @file
 * Custom plugin to provide an argument handler for tabs that allows you to choose multiple content types.
 */

$plugin = array(
  'title' => t("MYMODULE Node Tab: Name"),
  'description' => t('Creates a node tab context from a tab machine name argument.'),
  'context' => 'MYMODULE_custom_node_tab_argument_context',
  'keyword' => 'MYMODULE_custom_node_tab',
  'default' => array(
    'node_type' => array(),
  ),
  'settings form' => 'MYMODULE_custom_node_tab_argument_settings_form',
);

/**
 * Context callback: Create the tab context.
 */
function MYMODULE_custom_node_tab_argument_context($arg = NULL, $conf = NULL, $empty = FALSE) {
  // If unset it wants a generic, unfilled context.
  if ($empty) {
    return ctools_context_create_empty('entity:node_tab');
  }
  // We can accept either an entity object or a pure id.
  if (is_object($arg)) {
    return ctools_context_create('entity:node_tab', $arg);
  }

  foreach ($conf['node_type'] as $type) {
    $tab = node_tab_load($type, $arg);
  }

  if (!$tab) {
    return FALSE;
  }

  return ctools_context_create('entity:node_tab', $tab);
}

/**
 * Settings form callback: returns the settings for this argument.
 */
function MYMODULE_custom_node_tab_argument_settings_form(&$form, $form_state, $conf) {
  $types = node_type_get_types();
  $options = array();
  foreach ($types as $type => $info) {
    $options[$type] = check_plain($info->name);
  }

  $form['settings']['node_type'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content type'),
    '#options' => $options,
    '#default_value' => $conf['node_type'],
    '#multiple' => TRUE,
    '#required' => TRUE,
  );

  return $form;
}
